#include <gtest/gtest.h>

#include <ringbuffer.hpp>

TEST(RingBuffer, Empty) {
  ringbuffer<char, 2> rb;
  EXPECT_EQ(true, rb.is_empty());
  EXPECT_EQ(false, rb.is_full());
}

TEST(RingBufferPut, Full) {
  ringbuffer<char, 2> rb;
  rb.put('a');
  rb.put('a');
  EXPECT_EQ(false, rb.is_empty());
  EXPECT_EQ(true, rb.is_full());
}

TEST(RingBufferPut, Filling) {
  ringbuffer<char, 2> rb;
  EXPECT_EQ(true, rb.has_space());
  rb.put('a');
  EXPECT_EQ(false, rb.is_empty());
  EXPECT_EQ(false, rb.is_full());
  EXPECT_EQ(true, rb.has_space());
  rb.put('b');
  EXPECT_EQ(false, rb.is_empty());
  EXPECT_EQ(true, rb.is_full());
  EXPECT_EQ(false, rb.has_space());
}

TEST(RingBufferPut, OverrideOld) {
  ringbuffer<char, 2> rb;
  rb.put('a');
  rb.put('b');
  rb.put('c');
  EXPECT_EQ('b', rb.get());
  EXPECT_EQ('c', rb.get());
  EXPECT_EQ(true, rb.has_space());
  EXPECT_EQ(false, rb.is_full());
  EXPECT_EQ(true, rb.is_empty());
}

TEST(RingBufferPut, WriteCursorOverridesReadCursor) {
  ringbuffer<char, 5> rb;
  rb.put('a');
  rb.put('b');
  rb.put('c');
  rb.put('d');
  rb.put('e');
  rb.put('f');
  rb.put('g');
  EXPECT_EQ('c', rb.get());
  EXPECT_EQ('d', rb.get());
  EXPECT_EQ('e', rb.get());
  EXPECT_EQ('f', rb.get());
  EXPECT_EQ('g', rb.get());
  EXPECT_EQ(rb.is_empty(), true);
}

TEST(RingBufferPut, IsFullOnOverflow) {
  ringbuffer<char, 5> rb;
  rb.put('a');
  rb.put('b');
  rb.put('c');
  rb.put('d');
  rb.put('e');
  rb.put('f');
  rb.put('g');
  EXPECT_EQ(rb.is_full(), true);
}

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
