#include <algorithm>
#include <cassert>
#include <cmath>
#include <limits>

template <class T>
class sll {
  using pointer = sll<T>*;
  using difference_type [[maybe_unused]] = std::ptrdiff_t;
  using iterator_category [[maybe_unused]] = std::forward_iterator_tag;

 public:
  pointer forward;
  T data;
};

template <class T>
class dll {
  using pointer = sll<T>*;

 public:
  pointer forward;
  pointer backward;
};

template <class T, const std::size_t N>
class ringbuffer {
 public:
  ringbuffer(ringbuffer&&) = delete;  // mov ctor, stack only
  ringbuffer() { init_sll_array(); }

  [[nodiscard]] bool is_full() const noexcept { return m_full; }
  [[nodiscard]] bool is_empty() const noexcept { return m_w == m_r && !m_full; }
  [[nodiscard]] bool has_space() const noexcept { return !is_full(); }

  [[maybe_unused]] [[nodiscard]] constexpr std::size_t capacity()
    const noexcept {
    return N;
  }

  void put(T&& data) noexcept {
    const bool has_space = !m_full;
    m_w->data = std::forward<T>(data);
    m_w = m_w->forward;
    m_r = !has_space ? m_w : m_r;
    m_full = m_w == m_r;
  }

  T get() noexcept {
    const bool empty = is_empty();
    const T tmp = m_r->data;
    m_r = m_r->forward;
    m_full = false;
    m_w = empty ? m_r : m_w;
    return tmp;
  }

 private:
  using pointer = sll<T>*;
  using reference = sll<T>&;

  sll<T> m_elements[N];

  /* Read and write cursor */
  pointer m_r{m_elements};
  pointer m_w{m_r};

  bool m_full{false};

  void init_sll_array() noexcept {
    std::for_each(std::begin(m_elements), std::end(m_elements),
                  [](reference e) { e.forward = std::next(&e); });
    m_elements[N - 1].forward = m_elements;  // set tail pointing to head
  }
};
