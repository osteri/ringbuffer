# Stack based ring buffer library for embedded systems

Very basic implementation of a stack based ring buffer. Not thread safe. Uses real single linked list data structure, not modulo version that is usually seen in other ring buffer implementations.

## Basic usage

```cpp
#include <iostream>
#include "ringbuffer.hpp"

int main() {
  ringbuffer<float, 4> rb;
  float i = 1.0f;
  while (rb.has_space())
    rb.put(i++);

  std::cout << "elements: ";
  while (!rb.is_empty())
    std::cout << rb.get() << " ";
  std::cout << '\n';
  return EXIT_SUCCESS;
}
```

See more usage examples in [test](https://gitlab.com/osteri/ringbuffer/blob/main/test/) and [example](https://gitlab.com/osteri/ringbuffer/blob/main/example/) directories. 

## Adding library as a git submodule in CMake

First clone the library:
```
git submodule add https://gitlab.com/osteri/ringbuffer.git
```
Then include this library in your main `CMakeLists.txt` like this:
```
add_subdirectory(ringbuffer)
target_link_libraries(main LINK_PUBLIC ringbuffer-lib)
```

## Build all

```
cmake -S . -B build -DBUILD_TESTS=ON -DBUILD_EXAMPLES=ON
cd build && make -j
```
