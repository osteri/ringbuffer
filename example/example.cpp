#include <iostream>
#include <ringbuffer.hpp>

int main() {
  ringbuffer<float, 4> rb;

  float i = 1.0f;
  while (!rb.is_full()) rb.put(i++);

  rb.put(i++);
  rb.put(i++);

  std::cout << "elements: ";
  while (!rb.is_empty()) std::cout << rb.get() << " ";
  std::cout << '\n';

  return EXIT_SUCCESS;
}
